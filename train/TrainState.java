package train;

import tunnel.Tunnel;

/**
 * Created by Босс on 21.01.2017.
 */
public interface TrainState {

    Tunnel drive(Train train, Tunnel tunnel);
}
