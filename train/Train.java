package train;

import org.apache.log4j.Logger;
import tunnel.PoolTunnel;
import tunnel.Tunnel;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Босс on 16.01.2017.
 */
public class Train extends Thread {

    private int idTrain;
    private String route = "";
    private TrainState trainState;
    private static PoolTunnel tunnels;

    private static Lock lock = new ReentrantLock();
    private static Condition condition = lock.newCondition();
    private static Logger logger = Logger.getLogger(Train.class);

    public Train(){}

    public Train(PoolTunnel tunnels, int id, String route){
        this.tunnels = tunnels;
        this.idTrain = id;
        this.route = route;
        trainState = new TrainBeforeTunnel();
    }

    public void run(){
        Tunnel tunnel = null;
        tunnel = trainState.drive(this, tunnel);
        if (tunnel != null) {
            changeState(this, tunnel);
        }
    }

    public int getIdTrain() {
        return idTrain;
    }

    public String getRoute() {
        return route;
    }

    public void changeState(Train train, Tunnel tunnel){
        trainState.drive(train, tunnel);
    }

    public class TrainBeforeTunnel implements TrainState {

        @Override
        public Tunnel drive(Train train, Tunnel tunnel) {
            System.out.println("Train #" + train.getIdTrain() + " to " + train.getRoute() + " drove up the tunnels");
            trainState = new TrainInsideTunnel();
            tunnel = tunnels.crossTunnel(train);
            return tunnel;
        }
    }


    public class TrainInsideTunnel implements TrainState {

        @Override
        public Tunnel drive(Train train, Tunnel tunnel) {
            try {
                lock.lock();
                System.out.println("Train #" + train.getIdTrain() + " to " + train.getRoute() +
                        " is driving inside the tunnel " + tunnel.getName());
                condition.await(tunnel.getTimeCrossing(), TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                logger.error("negative argument:", e);
            }
            finally{
                lock.unlock();
                trainState = new TrainAfterTunnel();
            }
            return tunnel;
        }
    }

    public class TrainAfterTunnel implements TrainState {

        @Override
        public Tunnel drive(Train train, Tunnel tunnel) {
            trainState = new TrainBeforeTunnel();
            System.out.println("Train #" + train.getIdTrain() + " to " + train.getRoute() +
                    " passed the tunnel " + tunnel.getName());
            tunnel.releaseTunnel(train);
            return tunnel;
        }
    }
}
