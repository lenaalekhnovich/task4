package train;

import file.FileWork;
import org.apache.log4j.Logger;
import tunnel.PoolTunnel;
import tunnel.Tunnel;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class TrainRunner {

    private static PoolTunnel pool;
    private static Logger logger = Logger.getLogger(TrainRunner.class);

    public static void init(){
        try {
            FileWork file = new FileWork("src/resources/file.txt");
            int timeCrossing = file.readTimeCrossing();
            int amountTrain = file.readAmountTrain();
            int amountTrainRoute = file.readAmountTrainRoute();
            Tunnel tunnelFirst = new Tunnel("first", timeCrossing, amountTrain, amountTrainRoute);
            Tunnel tunnelSecond = new Tunnel("second", timeCrossing, amountTrain, amountTrainRoute);
            pool = PoolTunnel.getInstance(tunnelFirst, tunnelSecond);
        } catch (FileNotFoundException e) {
            logger.error("negative argument:", e);
        } catch (UnsupportedEncodingException e) {
            logger.error("negative argument:", e);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String train = "";
        int number = 1;
        init();
        System.out.println("n - run train to north, s - run train to south, m - run many trains, q - quite");
        while(!train.equals("q")){
            train = scanner.nextLine();
            switch(train){
                case "n":
                    new Train(pool, number++, "north").start();
                    break;
                case "s":
                    new Train(pool, number++ , "south").start();
                    break;
                case "m":
                    for (int i = 0; i < 5; i++){
                        new Train(pool, number++, "north").start();
                        new Train(pool, number++ , "south").start();
                    }
                    break;
            }
        }
    }
}
