package exception;

/**
 * Created by Босс on 23.01.2017.
 */
public class WrongRouteException extends Exception {

    public WrongRouteException(){
        super();
    }

    public WrongRouteException(String message){
        super(message);
    }

    public WrongRouteException(String message, Throwable cause) {
        super(message, cause);
    }

}
