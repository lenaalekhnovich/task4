package test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import train.Train;
import tunnel.PoolTunnel;
import tunnel.Tunnel;

/**
 * Created by Босс on 24.01.2017.
 */
public class TrainRunnerTest {

    private static PoolTunnel pool;

    @BeforeClass
    public static void init(){
        pool = PoolTunnel.getInstance(new Tunnel("first", 3, 3, 2), new Tunnel("second", 3, 3, 2));
    }

    @Test
    public void startTrainTest(){
        Train train = new Train(pool, 1, "south" );
        Tunnel tunnel = pool.crossTunnel(train);
        Assert.assertNotNull(tunnel);
        train.changeState(train,tunnel);
    }

    @Test
    public void chooseRouteTest(){
        Train train = new Train(pool, 1, "north" );
        Tunnel tunnel = new Tunnel("first", 5, 3, 1);
        Assert.assertNotNull(tunnel.chooseRoute(train));
    }

    @Test
    public void wrongTrainRouteTest(){
        Train train = new Train(pool, 1, "west" );
        Tunnel tunnel = new Tunnel("first", 4, 2, 2);
        Assert.assertNull(tunnel.chooseRoute(train));
    }

    @Test
    public void checkAmountTrainTest(){
        Tunnel tunnel = new Tunnel("tunnel", 3,2,1);
        Train train = new Train(pool, 1, "south" );
        Assert.assertTrue(tunnel.isFree(train));
    }
}
