package test;

import file.FileWork;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

/**
 * Created by Босс on 24.01.2017.
 */
public class FileWorkTest {

    private static FileWork fileWork;

    @Test
    public void readTimeCrossingTest() throws FileNotFoundException, UnsupportedEncodingException {
        fileWork = new FileWork("src/resources/file.txt");
        int time = fileWork.readTimeCrossing();
        Assert.assertNotEquals(time, 0);
    }

    @Test
    public void readAmountTrainsTest() throws FileNotFoundException, UnsupportedEncodingException {
        fileWork = new FileWork("src/resources/file.txt");
        int amount = fileWork.readAmountTrain();
        Assert.assertNotEquals(amount, 0);
    }

    @Test
    public void readAmountTrainsRouteTest() throws FileNotFoundException, UnsupportedEncodingException {
        fileWork = new FileWork("src/resources/file.txt");
        int amount = fileWork.readAmountTrainRoute();
        Assert.assertNotEquals(amount, 0);
    }

}
