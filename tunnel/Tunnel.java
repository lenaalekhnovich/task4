package tunnel;

import exception.WrongRouteException;
import org.apache.log4j.Logger;
import train.Train;

import java.util.concurrent.Semaphore;

/**
 * Created by Босс on 16.01.2017.
 */
public class Tunnel {

    private String name = "";
    private int timeCrossing;
    private Route routeToSouth = null;
    private Route routeToNorth = null;

    private final Semaphore tunnelSemaphore;
    private static Logger logger = Logger.getLogger(Tunnel.class);

    public Tunnel(String name, int timeCrossing, int amountTrain, int amountTrainRoute) {
        this.name = name;
        this.timeCrossing = timeCrossing;
        tunnelSemaphore = new Semaphore(amountTrain, true);
        routeToSouth = new Route(amountTrainRoute);
        routeToNorth = new Route(amountTrainRoute);
    }

    public String getName() {
        return name;
    }

    public int getTimeCrossing() {
        return timeCrossing;
    }

    public Tunnel chooseRoute(Train train) {
        Tunnel tunnel = null;
        boolean checkPassing;
        String route = train.getRoute();
        try {
            switch (route) {
                case "south":
                    checkPassing = routeToSouth.allowPassing();
                    break;
                case "north":
                    checkPassing = routeToNorth.allowPassing();
                    break;
                default:
                    throw new WrongRouteException("Wrong route of the train");
            }
            if(checkPassing) {
                tunnelSemaphore.acquire();
            }
            return this;
        } catch (InterruptedException e) {
            logger.error("negative argument:", e);
        } catch (WrongRouteException e) {
            logger.error("negative argument:", e);
        }
        return tunnel;
    }

    public boolean isFree(Train train){
        Route route;
        try {
            switch (train.getRoute()) {
                case "south":
                    route = routeToSouth;
                    break;
                case "north":
                    route = routeToNorth;
                    break;
                default:
                    throw new WrongRouteException("Wrong route of the train");
            }
            return tunnelSemaphore.availablePermits() != 0 && route.isFree() ? true : false;
        } catch (WrongRouteException e) {
            logger.error("negative argument:", e);
        }
        return false;
    }

    public void releaseTunnel(Train train) {
        try {
            switch (train.getRoute()) {
                case "south":
                    routeToSouth.releaseRoute();
                    break;
                case "north":
                    routeToNorth.releaseRoute();
                    break;
                default:
                    throw new WrongRouteException("Wrong route of the train");
            }
        } catch (WrongRouteException e) {
            logger.error("negative argument:", e);
        }
        tunnelSemaphore.release();
    }

}
