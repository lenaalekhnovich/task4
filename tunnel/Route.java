package tunnel;

import org.apache.log4j.Logger;

import java.util.concurrent.Semaphore;

/**
 * Created by Босс on 19.01.2017.
 */
public class Route {

    private final Semaphore routeSemaphore;

    private static Logger logger = Logger.getLogger(Route.class);

    public Route(int amount){
        routeSemaphore = new Semaphore(amount, true);
    }

    public boolean allowPassing() {
        try {
            routeSemaphore.acquire();
            return true;
        } catch (InterruptedException e) {
            logger.error("negative argument:", e);
        }
        return false;
    }

    public void releaseRoute(){
        routeSemaphore.release();
    }

    public boolean isFree(){
        return routeSemaphore.availablePermits() != 0 ? true : false;
    }

}
