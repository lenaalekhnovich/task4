package tunnel;

import org.apache.log4j.Logger;
import train.Train;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Босс on 16.01.2017.
 */
public class PoolTunnel extends Thread{

    private Tunnel firstTunnel = null;
    private Tunnel secondTunnel = null;
    private Train trainInTunnel = null;

    private static Lock lock = new ReentrantLock();
    private static Condition condition = lock.newCondition();
    private static PoolTunnel instance = null;

    private static Logger logger = Logger.getLogger(PoolTunnel.class);

    public static PoolTunnel getInstance(Tunnel tunnelFirst, Tunnel tunnelSecond) {
        lock.lock();
        try {
            if (instance == null) {
                instance = new PoolTunnel(tunnelFirst, tunnelSecond);
            }
        } finally {
            lock.unlock();
        }
        return instance;
    }

    private PoolTunnel(Tunnel firstTunnel, Tunnel secondTunnel){
        this.firstTunnel = firstTunnel;
        this.secondTunnel = secondTunnel;
        trainInTunnel = new Train();
    }

    public Tunnel crossTunnel(Train train) {
        Tunnel tunnel = null;
        try {
            lock.lock();
            while (!firstTunnel.isFree(train) && !secondTunnel.isFree(train)) {
                condition.await(firstTunnel.getTimeCrossing(), TimeUnit.SECONDS);
            }
            train.setPriority(train.getRoute().equals(trainInTunnel.getRoute()) ? MAX_PRIORITY : NORM_PRIORITY);
            trainInTunnel = train;
            tunnel = firstTunnel.isFree(train) ? firstTunnel.chooseRoute(train) : secondTunnel.chooseRoute(train);
            condition.signalAll();
        } catch (InterruptedException e) {
            logger.error("negative argument:", e);
        } finally {
            lock.unlock();
        }
        train.changeState(train, tunnel);
        return tunnel;
    }
}
