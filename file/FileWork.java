package file;

import org.apache.log4j.Logger;

import java.io.*;

/**
 * Created by Босс on 18.12.2016.
 */
public class FileWork {

    private FileInputStream inF;
    private BufferedReader reader;

    static Logger logger = Logger.getLogger(FileWork.class);

    public FileWork(String fileName) throws FileNotFoundException, UnsupportedEncodingException {
        inF = new FileInputStream(fileName);
        reader = new BufferedReader(new InputStreamReader(inF, "UTF-8"));
    }

    public int readAmountTrain(){
        int amount = 0;
        String str;
        try {
            while((str = reader.readLine())!= null) {
                if(str.contains("AmountTrain ")) {
                    amount = Integer.parseInt(str.substring(str.indexOf(" ")).trim());
                    break;
                }
            }
        } catch (NumberFormatException e){
            logger.error("negative argument:", e);
        } catch (IOException e) {
            logger.error("negative argument:", e);
        }
        return amount;
    }

    public int readAmountTrainRoute(){
        int amount = 0;
        String str;
        try {
            while((str = reader.readLine())!= null) {
                if(str.contains("AmountTrainRoute")) {
                    amount = Integer.parseInt(str.substring(str.indexOf(" ")).trim());
                    break;
                }
            }
        } catch (NumberFormatException e){
            logger.error("negative argument:", e);
        } catch (IOException e) {
            logger.error("negative argument:", e);
        }
        return amount;
    }

    public int readTimeCrossing(){
        int time = 0;
        String str;
        try {
            while((str = reader.readLine()) != null) {
                if(str.contains("TimeCrossing")) {
                    time = Integer.parseInt(str.substring(str.indexOf(" ")).trim());
                    break;
                }
            }
        } catch (NumberFormatException e){
            logger.error("negative argument:", e);
        } catch (IOException e) {
            logger.error("negative argument:", e);
        }
        return time;
    }
}
